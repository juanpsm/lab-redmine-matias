redmine
=========

Este rol es el encargado de instalar y configurar la aplicacion redmine. Esta tarea
se lleva a cabo ejecutando distintos conjuntos de tasks agrupados en diferentes archivos:

[ruby](./tasks/ruby.yml)

Este conjunto de tasks se encarga de instalar RVM, instalar ruby, instalar rails y setear 
la version de ruby por defecto

[postgres](./tasks/posgres.yml)

Este conjunto de tasks se encarga de instalar postgresql. Adicionalmente crea la base de datos y
el usuario que utilizara la aplicacion; asi como tambien establece que la base de datos sea solo
accesible desde dentro de la maquina.

[redmine](./tasks/redmine.yml)

Este conjunto de tasks se encarga de traer el codigo fuente, instalar dependencias, configurar
la aplicacion (configurar la conexion a la base de datos, setear la firma de las cookies, correr
migraciones... etc).

[servers](./tasks/servers.yml)

Se encarga de instalar y configurar los servidores de aplicacion y web; para finalmente dejar disponible
la aplicacion.

Se desarrollo utilizando TDD mediante Molecule. Para correr los test:

```sh
cd provision/roles/redmine/

molecule test
```

Variables
--------------

| nombre | valor por defecto | descripcion |
| -- | --- | --- |
| ruby\_version | 2.6.6 | La version de ruby que se va a instalar en la VM |
| rails\_version | 5.2 | La version de rails que se va a instalar en la VM |
| redmine\_version | 4.0.9 | La version de redmine se va a instalar en la VM |
| redmine\_dbuser | redmine | El usuario que utilizara la aplicacion para interactuar con la BD |
| redmine\_dbpasswd | redmine | El password que utilizara el usuario anterior para acceder a la BD |

Ejemplo
----------------

```yml
    - hosts: servers
      roles:
        - role: redmine
          vars: 
            redmine_dbuser: unusuario
            redmine_dbpasswd: unapass
```
