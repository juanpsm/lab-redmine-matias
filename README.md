# Laboratorio 4 - Redmine

## Descripcion

Se utiliza vagrant para crear una maquina virtual por defecto que
es aprovisionada mediante ansible que tiene mapeado el puerto 80 en
el puerto 8000 del guest. Entonces una vez levantada y aprovisionada
al dirigirnos a localhost:8000 deberiamos ver la aplicacion redmine
corriendo en dicha MV.

Para ello se creo un role llamado redmine que ejecuta 
Se desarrollo la solucion siguiendo TDD utilizando el Molecule.


## Set up

Clonamos el repo:

```sh
git clone git@gitlab.com:chudix/lab4-redmine.git
```

Instalamos dependencias:

```sh
pip install requirements.txt
```

Finalmente levantamos la maquina virtual:

```sh
vagrant up
```

En localhost:8000 podemos ver la aplicacion corriendo.

## Solucion

Para la solucion se creo el role redmine que se encarga de ejecutar los tasks
correspondientes para instalar redmine.

* Dicho rol instala RVM para manejar la version de ruby e instala rails

* Instala postgresql13 e lo inicializa para que sea utilizado por la aplicacion.

* Instala el codigo de redmine y lo configura.

* Instala como servidor web Apache y como app server Passenger.

Para mas informacion ver [redmine](./provision/roles/redmine/README.md).

